@Metadata.layer: #CORE

@UI: { headerInfo: { typeName: 'Travel',
                     typeNamePlural: 'Travels',
                     title: { type: #STANDARD, value: 'TravelID' } } }
annotate entity ZRAPH_##_C_TravelWDTP with
{
  // @UI.facet: [{ ... }]

  @UI: { lineItem:       [ { position: 10, importance: #HIGH, label: 'Travel ID' },
                           { type: #FOR_ACTION, dataAction: 'acceptTravel', label: 'Accept' } ],
         identification: [ { position: 10, label: 'Travel ID' } ],
         selectionField: [ { position: 10 } ] }
  TravelID;
// ...

}