# Eclipse - ADT Keyboard Shortcuts

## Most important shortcuts

| Keyboard Shortcut    | Function |
| ----------- | ----------- |
| `Ctrl + F3`     | Activate inactive development object|
| `Ctrl + Shift F3`     | Activate all inactive development objects|
| `Ctrl + Space`     | Code completion / Content Assist|
| `Shift + F1`     | Format source code (_Pretty Printer_)|
| `Ctrl + 1`     | Quick Fix / Quick Assist|
| `Ctrl + Shift + A`     | Open development object (_Search_)|
| `F2`     | Show ABAP element info|
| `F8`     | Run ABAP Object (_CDS / Table Data Preview_)|
| `F9`     | Run ABAP Application (Console) (_Execute Class, etc._)|
| `Alt + F8`     | Run ABAP Development Object (_SAP GUI Transactions_)|

For the full list of Eclipse ADT shortcuts, please visit [SAP Help](https://help.sap.com/doc/saphelp_nw75/7.5.5/en-US/4e/c299d16e391014adc9fffe4e204223/content.htm?no_cache=true).