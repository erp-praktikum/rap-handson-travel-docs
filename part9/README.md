# Part 9 - Extending the SAP Fiori List Report
## Table of Contents
* [Previous: 7. Business Application Studio](../part7/7a.md)  
* [Previous: 8. Deploying our app (optional)](../part8/README.md)  
* **Current: 9. Extending the SAP Fiori List Report**  
    * [Requirement](#markdown-header-requirement)  
    * [1. Add the custom filter](#markdown-header-1-add-the-custom-filter)    
    * [2. Add custom action](#markdown-header-2-add-custom-action)  
    * [3. Add a custom column to the table](#markdown-header-3-add-a-custom-column-to-the-table)  
    * [4. Let us use the extensionAPI for once](#markdown-header-4-let-us-use-the-extensionapi-for-once)  
* [Next: 10. CDS Custom Entity (optional)](../part10/1.CustomEntityIntro.md)

## Requirement
We want to add the absolutely useful function to open 3 predefined web pages by selecting them in a dropdown box and pressing a button.  
For this, we will need to implement 2 extensions to the list report, one filter and one action.  
This is covered in section 1 and 2.  
  
Then we also want to add a new column to the list report, which contains a button that tells us the agency name of the current line.  
This is covered in section 3.  
  
At last we want a selection dependent multi select action that tells us which agencies are in the selected rows.  
This is covered in section 4.  
  
## 1. Add the custom filter
Open the guided development by pressing CTRL+SHIFT+P and choose "Fiori: Open Guided Development" or right click on the project and select the item "Open Guided Development".  
  
In section "List Report Page" select item "Add a custom filter to the filter bar".  
Press "Start Guide".  
In the wizard select/enter the following information:  

| Field              | Value         |
| ------------------ | ------------- |
| Fragment File Name | CustomFilter  |
| Custom Filter Key  | customFilter  |
| Custom Filter Name | Custom Filter |
| Control ID         | customFilter  |
  
Press "Create a File and Insert Snippet".  
You can close the tab that opens to the right.  
Press "Next".  
  
In the next step enter the following information:  

| Field                  | Value                 |
| ---------------------- | --------------------- |
| Custom Filter Property | customFilterProperty  |
  
Press "Create or Update a File and Insert Snippet".  
You can close the tab that opens to the right.  
Press "Next".  
  
In the next step enter the following information:  

| Field      | Value   |
| ---------- | ------- |
| Entity Set | Travel  |
  
Press "Insert Snippet".  
You can close the tab that opens to the right.  
Press "Exit Guide".  
  
In the folder webapp find the file manifest.json.  
Within this file, find the entry "flexEnabled" in section "sap.ui5".  
Change the entry from "flexEnabled": true to "flexEnabled": false.  
This will disable certain error messages regarding missing ids.  
It is not important for the exercise why this is, but you are welcome to ask the trainer.  
  
In webapp/ext/fragments find the file CustomFilter.fragment.xml.  
You will find 3 generated XML entries with this pattern:  
```xml
<core:Item key="0" text="Item1"/>
```
  
In this example, 0 is the key and Item1 is the text as they are assigned the corresponding properties.  
The double quotes (") are just needed for proper XML syntax.  
Change the texts of the the 3 items within the ComboBox to 3 non-suspicious web pages.  
Example:  

| Item key | Item text                                                                           |
| -------- | ----------------------------------------------------------------------------------- |
| 0        | https://www.msg.group/en                                                            |
| 1        | https://www.uni-passau.de/en/                                                       |
| 2        | https://sapui5.hana.ondemand.com/sdk/#/api/sap.fe.templates.ListReport.ExtensionAPI |
  
Test the app and see the new filter "Custom Filter" that looks like this:  
![Custom Filter Field](images/image2.png)
  
[Solution](solutions/CustomFilter.fragment.xml)  
  
## 2. Add custom action
Open the guided development by pressing CTRL+SHIFT+P and choose "Fiori: Open Guided Development" or right click on the project and select the item "Open Guided Development".  
  
Select item "Add a custom action to a page using extensions" in "List Report Page".  
Press "Start Guide".  
In the wizard select/enter the following information:  

| Field         | Value                    |
| ------------- | ------------------------ |
| Page Type     | List Report Page         |
| Function Name | onExtensionButtonPressed |
  
Press "Create or Update a File and Insert Snippet".  
You can close the tab that opens to the right.  
Press "Next".  
  
In the next step select/enter the following information:  

| Field           | Value         |
| --------------- | ------------- |
| Entity Set      | Travel        |
| Action Position | Table Toolbar |
| Action ID       | customAction  |
| Button Text     | Custom Action |
| Row Selection   | no            |
  
Press "Insert Snippet".  
You can close the tab that opens to the right.  
Press "Exit Guide".  
  
### Test the App
Find the new button in the toolbar:  
![Custom Action Button](images/image1.png)
  
If you press on it, you will not yet see anything happen.  
While there is a default behaviour generated, it contains an error as of now.  
  
### Add own coding to open an external web page
Find the generated controller "ListReportExt.controller.js" in webapp/ext/controller.  
Within this file, the function "onExtensionButtonPressed" exists, which we will now change.  

Replace the functions content (everything within the curly braces {}) with:  
```js
const sExternalPage = this.getView().byId("customFilter").getValue();
window.open(sExternalPage);
```
  
Again test the button. Now the selected web page is opened (or an empty tab/window if nothing is selected).  
  
[Solution](solutions/ListReportExt.controller-1.js)  
  
## 3. Add a custom column to the table

### Add basic column
Start the guide "Add custom columns to the table using extensions" in "List Report Page".  
In the wizard select/enter the following information:  

| Field            | Value      |
| ---------------- | ---------- |
| Table Type       | Responsive |
| Entity Set       | Travel     |
| Leading Property | AgencyName |
  
Press "Create a File and Insert Snippet".  
You can close the tab that opens to the right.  
Press "Next".  
  
In this step we don't have any input to give.  
  
Press "Create a file and Insert Snippet".  
You can close the tab that opens to the right.  
Press "Next".  
  
In this step, enter the following information:  

| Field      | Value       |
| ---------- | ----------- |
| Page Type  | List Report |
| Entity Set | Travel      |
  
Press "Insert Snippet".  
You can close the tab that opens to the right.  
Press "Exit Guide".  
  
Find file ResponsiveTableColumns.fragment.xml in webapp/ext/fragments.  
In the file, change the columnIndex value from 101 to 2.  
this value is a bit hidden between `&quot;` codes, so here is a little help.  
before:  
```xml
<core:CustomData key="p13nData" value="\{&quot;columnKey&quot;: &quot;CustomColumn1&quot;, &quot;leadingProperty&quot;: &quot;TravelID&quot;, &quot;columnIndex&quot;: &quot;101&quot;}"/>
```
after:  
```xml
<core:CustomData key="p13nData" value="\{&quot;columnKey&quot;: &quot;CustomColumn1&quot;, &quot;leadingProperty&quot;: &quot;TravelID&quot;, &quot;columnIndex&quot;: &quot;2&quot;}"/>
```
  
Check the app. Select data by pressing the "Go" button and see the custom cells.  
It should look sort of like this:  
![Custom Column](images/image3.png)  
  
### Give some life to the custom column
  
#### Make a button with context dependent text
Find ResponsiveTableCells.fragment.xml in webapp/ext/fragments.  
We want to include the name of the hotel in a button text and handle the press on it later.  
Replace the Text control with a button control.  
This is done by replacing "Text" with "Button" within the XML Tags.  
The property "text" is already defined, but "press" must be added by you.  
Remember that in XML values for properties need to be within double quotes.  
It must have the following properties:  

| Property | Value                        |
| -------- | ---------------------------- |
| text     | Show info for `{AgencyName}` |
| press    | onRowButtonPressed           |
  
Check the app. Select data to see the custom cells.  
It should now look sort of like this:  
![Custom Column](images/image4.png)  
  
[Solution](solutions/ResponsiveTableCells.fragment.xml)  

#### Handle the button press
In the previous step we already told the framework to call a function called "onRowButtonPressed", when the button is pressed.  
However we did not yet define that function.  
So now we open webapp/ext/controller/ListReportExt.controller.js and add the following function definition:  

```js
onRowButtonPressed: function(oEvent) {
    const sAgencyName = oEvent.getSource().getBindingContext().getProperty("AgencyName");
    sap.m.MessageToast.show(sAgencyName + " was pressed");
}
```
  
When the button is now pressed, the result should look like this:  
![Custom Column](images/image5.png)  
  
[Solution](solutions/ListReportExt.controller-2.js)  
  
## 4. Let us use the extensionAPI for once

### Add another custom action
Open the guided development by pressing CTRL+SHIFT+P and choose "Fiori: Open Guided Development" or right click on the project and select the item "Open Guided Development".  
  
Select item "Add a custom action to a page using extensions" in "List Report Page".  
Press "Start Guide".  
In the wizard select/enter the following information:  

| Field         | Value                     |
| ------------- | ------------------------- |
| Page Type     | List Report Page          |
| Function Name | onExtensionButton2Pressed |
  
Press "Create or Update a File and Insert Snippet".  
Press "Next".  
  
In the next step select/enter the following information:  

| Field           | Value             |
| --------------- | ----------------- |
| Entity Set      | Travel            |
| Action Position | Table Toolbar     |
| Action ID       | customAction2     |
| Button Text     | What is selected? |
| Row Selection   | yes               |
  
Press "Insert Snippet".  
Press "Exit Guide".  
  
### Adapt the coding
In webapp/ext/controller/ListReportExt.controller.js find function onExtensionButton2Pressed.  
Adapt it to show a message toast with the text "`<AgencyNames>` were selected", where `<AgencyName>` should be replaced with the comma separated list of selected agencies.  
Note: you need to call the message toast like in the previous extension button handler with "sap.m.MessageToast".  
  
Within the function you can access the extension API using:  
```js
this.extensionAPI
```
Here is a helpful resource as how to get the selected contexts:  
[ExtensionAPI](https://sapui5.hana.ondemand.com/sdk/#/api/sap.suite.ui.generic.template.ListReport.extensionAPI.ExtensionAPI%23methods/getSelectedContexts)  
Call this method and assign the result to a variable aSelection:  
```js
let aSelection = // insert your code to determine the selected contexts here
```
  
A selected context can access the data of the corresponding line using the getProperty method as already done in onRowButtonPressed.  
  
To concatenate the values of the resulting array, the reduce method can be used:    
[Array.reduce](https://www.w3schools.com/jsref/jsref_reduce.asp)  
This method takes a function as first and an initial value as second parameter.  
The function to be given as first parameter can be declared an passed as follows.  
```js
const fnReduction = function(sValue, oContext) {
    let sCurrentValue = // insert your code to determine the Agency name out of the context oContext here
    if(sValue.length === 0) {
        return sCurrentValue;
    } else {
        return sValue + ", " + sCurrentValue;
    }
};
let sMessageText = aSelection.reduce(fnReduction, "") + " were selected";
```
  
[Solution](solutions/ListReportExt.controller-3.js)  
  
### Activate multiselect
Open the guided development item "Configure multiple selection for a table" in "List Report Page".  
As Page Name, choose the ListReport_Travel.  
Set "Toggle Multi Select" to "True".  
  
Press "Insert Snippet".  
Press "Exit Guide".  
  
[Solution manifest.json](solutions/manifest.json)  

### Test the app
It should now look like this:  

![Custom Column](images/image6.png)
![Custom Column](images/image7.png)  

## Next step
[10. CDS Custom Entity (optional)](../part10/1.CustomEntityIntro.md)