# Part 3e - Calculating Virtual Elements
## Table of Contents
* [Previous: 2. Creating and filling the Database Tables](../part2/README.md)
* [Previous: 3a. Creating the Virtual Data Model (VDM) via ABAP CDS Views](3a.md)  
* [Previous: 3b. Adding master data associations to the VDM](3b.md)
* [Previous: 3c. Projection Views](3c.md)  
* [Previous: 3d. Creating Metadata Extensions](3d.md) 
* **Current: 3e. Calculating Virtual Elements**  
    * [Introduction](#markdown-header-introduction)  
    * [Adding a virtual element](#markdown-header-adding-a-virtual-element)      
* [Next: 4. Behavior Definition & Projection](../part4/4a.md)

## Introduction
[^ Top of page](#)  
Virtual Elements are used to define additional CDS fields that are not persisted on the database. Instead, they are calculated during runtime using ABAP classes that implement the virtual element interface. Virtual elements represent transient fields in business applications. They are defined at the level of CDS projection views as additional elements within the `SELECT` list.  

However, the calculation of their values is carried out by means of ABAP classes that implement the specific virtual element interface provided for this purpose. The ABAP implementation class is referenced by annotating the virtual element in the CDS projection view with 
`@ObjectModel.virtualElementCalculatedBy: 'ABAP:<CLASS_NAME>'`.  

Like the `calculation` of virtual elements, `filtering` and `sorting` must be implemented manually in dedicated ABAP implementation classes - though the latter two will be ignored here due to technical restrictions.  
 
**Remark** Virtual elements have some rules/limitations like:

* Aliases for virtual elements are not allowed  
* You can use virtual elements only in CDS projection views
* Virtual elements cannot be keys of the CDS projection view  
* Virtual elements cannot be used for grouping or aggregating  
* Data from virtual elements can only be retrieved via the query framework. In particular this means that the following options to retrieve data from CDS are not possible for virtual elements:
    * ABAP SQL SELECT on CDS views returns initial values for the virtual element
    * EML READ on BO entities is not possible as EML does not know the virtual elements

```abap
define view entity CDSProjView
  as projection on CDSEntity
{
...
           @ObjectModel.virtualElementCalculatedBy: 'ABAP:ABAPClass'
  virtual  ElemName : { DataElement | ABAPType } ,
...
}
```

For more information, see [Virtual Elements](https://help.sap.com/viewer/fc4c71aa50014fd1b43721701471913d/202009.000/en-US/df0ef4aac2d34fdd948b8a8883df3a1f.html).   

### Adding a virtual element
[^ Top of page](#)  
We want to create a virtual element on the booking object page ( `ZRAPH_##_C_BookingWDTP` ). The field `DaysToFlight` will calculate how many days are left before the flight or how many days have passed since the flight, so we compare the value of `FlightDate` with today's date to calculate the value for the virtual element.  

**Steps to implement the logic**  

* Open the Travel projection view `ZRAPH_##_C_BookingWDTP`, add the virtual element with type `abap.int2` and abovementioned annotation. 
* Add the field to metadata extension and provide the UI annotations needed to display the field on the list report and object page.
* Create the class you added in `@ObjectModel.virtualElementCalculatedBy:'ABAP:ZRAPH_##_CL_DAYS_TO_FLIGHT'` 
* Add the interface `IF_SADL_EXIT_CALC_ELEMENT_READ`.
* Implement the methods `get_calculation_info` and`calculate` of the abovementioned interface:

    * [get_calculation_info](https://help.sap.com/viewer/fc4c71aa50014fd1b43721701471913d/202009.000/en-US/4430bddf68ee4258bf629759f0ff6ab5.html#loio4430bddf68ee4258bf629759f0ff6ab5__section_get_calulation_info): This method has to request other fields which are necessary for the calculation. Therefore you'll have to loop over the incoming table `it_requested_calc_elements` and in case `DAYSTOFLIGHT` is included there we have to append `FLIGHTDATE` to the exporting table `et_requested_orig_elements`. This tells the framework that we additionally need the booking's flight date in order to calculate the virtual element.
    * [calculate](https://help.sap.com/viewer/fc4c71aa50014fd1b43721701471913d/202009.000/en-US/4430bddf68ee4258bf629759f0ff6ab5.html#loio4430bddf68ee4258bf629759f0ff6ab5__section_calculate): Here we actually calculate the difference of the flight date and the current date to retrieve the amount of days in between. To do so, store the current date via `cl_abap_context_info=>get_system_date( )` in a variable and map the incoming data via `CORRESPONDING #( it_original_data )` to an internal table with the type `zraph_##_c_bookingwdtp`. Next, loop over your table assigning a field symbol. Here you can now set the field `daystoflight` using the booking's flight date and today's date. Finally, add your table (again via `CORRESPONDING #`) to the exporting parameter `ct_calculated_data`. 

**Solution** [ZRAPH_##_CL_Days_To_Flight](sources/Z_CL_Days_To_Flight.txt)

## Next step
[^ Top of page](#)  
[4. Behavior Definition & Projection](../part4/4a.md)