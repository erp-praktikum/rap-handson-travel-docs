@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'RAP HandsOn: RoomReservation (Transactnl)'
define view entity ZRAPH_##_I_RoomReservationWDTP
  as select from zraph_##_roomrsv

  association        to parent ZRAPH_##_I_TravelWDTP as _Travel     on $projection.TravelUUID = _Travel.TravelUUID

{
  key roomrsv_uuid         as RoomRsvUUID,

      parent_uuid           as TravelUUID,

      roomrsv_id            as RoomRsvID,

      hotel_id              as HotelID,

      begin_date            as BeginDate,

      end_date              as EndDate,

      room_type             as RoomType,

      @Semantics.amount.currencyCode: 'CurrencyCode'
      roomrsv_price         as RoomRsvPrice,

      currency_code         as CurrencyCode,

      //local ETag field --> OData ETag
      @Semantics.systemDateTime.localInstanceLastChangedAt: true
      local_last_changed_at as LocalLastChangedAt,

      //Associations
      _Travel
}
